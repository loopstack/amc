package cn.icanci.loopstack.amc.biz.model;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:45
 */
public class AppMockDebugResult implements Serializable {
    private static final long serialVersionUID = 5879082736696321315L;
    /**
     * 是否执行成功
     */
    private boolean           success;
    /**
     * 脚本类型
     */
    private String            scriptType;
    /**
     * 实际执行结果
     */
    private Object            realResult;
    /**
     * 执行过程中出现的异常
     */
    private String            exceptionMessage;

    public AppMockDebugResult() {
    }

    public AppMockDebugResult(boolean success, String scriptType, Object realResult, String exceptionMessage) {
        this.success = success;
        this.scriptType = scriptType;
        this.realResult = realResult;
        this.exceptionMessage = exceptionMessage;
    }

    public static AppMockDebugResult fail(String scriptType, String localizedMessage) {
        AppMockDebugResult result = new AppMockDebugResult();
        result.setSuccess(false);
        result.setScriptType(scriptType);
        result.setExceptionMessage(localizedMessage);
        return result;
    }

    public static AppMockDebugResult success(String scriptType, Object realResult) {
        AppMockDebugResult result = new AppMockDebugResult();
        result.setSuccess(true);
        result.setScriptType(scriptType);
        result.setRealResult(realResult);
        return result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public Object getRealResult() {
        return realResult;
    }

    public void setRealResult(Object realResult) {
        this.realResult = realResult;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
