package cn.icanci.loopstack.amc.biz.service.impl;

import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.AppMockCallReportDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallReportDO;
import cn.icanci.loopstack.amc.biz.mapper.AppMockCallReportMapper;
import cn.icanci.loopstack.amc.biz.service.AppMockCallReportService;
import cn.icanci.loopstack.amc.common.enums.BooleanEnum;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.report.AppMockCallReportVO;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/18 11:49
 */
@Service
public class AppMockCallReportServiceImpl implements AppMockCallReportService {
    @Resource
    private AppMockCallReportDAO    appMockCallReportDAO;
    @Resource
    private AppMockCallReportMapper appMockCallReportMapper;

    @Override
    public PageList<AppMockCallReportVO> queryPage(AppMockCallReportVO vo, int currentPage, int pageSize) {
        PageList<AppMockCallReportDO> pageQuery = appMockCallReportDAO.pageQuery(appMockCallReportMapper.vo2do(vo), currentPage, pageSize);
        return new PageList<>(appMockCallReportMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public List<AppMockCallReportVO> queryAll(AppMockCallReportVO vo) {
        return appMockCallReportMapper.dos2vos(appMockCallReportDAO.queryAllByReport(appMockCallReportMapper.vo2do(vo)));
    }

    @Override
    public void deleteById(String id) {
        AppMockCallReportDO reportDO = appMockCallReportDAO.queryOneById(id);
        if (reportDO == null) {
            return;
        }
        reportDO.setIsDelete(BooleanEnum.YES.getCode());
        appMockCallReportDAO.update(reportDO);
    }
}
