package cn.icanci.loopstack.amc.biz.model;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 21:29
 */
public class PublishResource implements Serializable {
    /**
     * 需要发布的资源
     */
    private String resource;
    /**
     * 发布的资源值
     */
    private String value;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}