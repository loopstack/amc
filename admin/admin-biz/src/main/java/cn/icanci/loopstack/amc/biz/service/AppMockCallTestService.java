package cn.icanci.loopstack.amc.biz.service;

import java.util.List;

import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppMockCallTestVO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 19:21
 */
public interface AppMockCallTestService {

    PageList<AppMockCallTestVO> queryPage(AppMockCallTestVO web2vo, int currentPage, int pageSize);

    void save(AppMockCallTestVO web2vo);

    AppMockCallTestVO validateAppMockCallName(String mockName);

    List<TextValue> loadSelector();
}
