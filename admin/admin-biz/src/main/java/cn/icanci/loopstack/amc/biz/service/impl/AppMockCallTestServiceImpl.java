package cn.icanci.loopstack.amc.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.AppMockCallTestDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallTestDO;
import cn.icanci.loopstack.amc.biz.event.log.LogEvent;
import cn.icanci.loopstack.amc.biz.mapper.AppMockCallTestMapper;
import cn.icanci.loopstack.amc.biz.service.AppMockCallTestService;
import cn.icanci.loopstack.amc.biz.service.BaseService;
import cn.icanci.loopstack.amc.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.amc.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppMockCallTestVO;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 19:21
 */
@Service
public class AppMockCallTestServiceImpl extends BaseService<AppMockCallTestVO> implements AppMockCallTestService {
    @Resource
    private AppMockCallTestDAO    appMockCallTestDAO;
    @Resource
    private AppMockCallTestMapper appMockCallTestMapper;

    @Override
    public PageList<AppMockCallTestVO> queryPage(AppMockCallTestVO web2vo, int currentPage, int pageSize) {
        PageList<AppMockCallTestDO> pageQuery = appMockCallTestDAO.pageQuery(appMockCallTestMapper.vo2do(web2vo), currentPage, pageSize);
        return new PageList<>(appMockCallTestMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public void save(AppMockCallTestVO appMockCallTest) {
        if (doInsert(appMockCallTest)) {
            AppMockCallTestDO appMockCallTestDO = appMockCallTestMapper.vo2do(appMockCallTest);
            appMockCallTestDAO.insert(appMockCallTestDO);
            eventDispatcher
                .fire(new LogEvent(appMockCallTestDO.getUuid(), ModuleTypeEnum.AMC_APP_MOCK_CALL_TEST, JSONUtil.toJsonStr(appMockCallTestDO), LogOperatorTypeEnum.CREATE));
        } else {
            appMockCallTestDAO.update(appMockCallTestMapper.vo2do(appMockCallTest));
            eventDispatcher.fire(new LogEvent(appMockCallTest.getUuid(), ModuleTypeEnum.AMC_APP_MOCK_CALL_TEST, JSONUtil.toJsonStr(appMockCallTest), LogOperatorTypeEnum.UPDATE));
        }
    }

    @Override
    public AppMockCallTestVO validateAppMockCallName(String mockName) {
        return appMockCallTestMapper.do2vo(appMockCallTestDAO.queryByAppMockName(mockName));
    }

    @Override
    public List<TextValue> loadSelector() {
        List<AppMockCallTestDO> appMockTestList = appMockCallTestDAO.queryAll();
        if (CollectionUtils.isEmpty(appMockTestList)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (AppMockCallTestDO appMockCallTestDO : appMockTestList) {
            String label;
            AppMockCallTestVO appMockCallTestVO = appMockCallTestMapper.do2vo(appMockCallTestDO);
            if (isDeleted(appMockCallTestVO)) {
                label = String.format(DELETED_FORMAT, appMockCallTestVO.getMockTestName());
            } else {
                label = String.format(NOT_DELETED_FORMAT, appMockCallTestVO.getMockTestName());
            }
            textValues.add(new TextValue(label, appMockCallTestVO.getTestJson()));
        }
        return textValues;
    }
}
