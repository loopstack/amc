package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.biz.model.AppMockDebugResult;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppMockCallVO;
import cn.icanci.loopstack.amc.common.result.R;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:04
 */
public interface AppMockCallService {

    PageList<AppMockCallVO> queryPage(AppMockCallVO web2vo, int currentPage, int pageSize);

    void save(AppMockCallVO web2vo);

    AppMockCallVO validateAppMockCallName(String mockName);

    List<TextValue> loadSelector();

    AppMockDebugResult debug(AppMockCallVO appMock, String scriptContentTest);

    R publishMockCall(String uuid);
}
