package cn.icanci.loopstack.amc.biz.mapper.converter;

import cn.icanci.loopstack.amc.common.enums.ModuleTypeEnum;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 15:10
 */
@Component
public class ModuleTypeEnumConverter extends AbstractBaseConverter<ModuleTypeEnum> {
}
