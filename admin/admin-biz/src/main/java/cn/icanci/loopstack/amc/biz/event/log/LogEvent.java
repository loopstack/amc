package cn.icanci.loopstack.amc.biz.event.log;

import cn.icanci.loopstack.amc.biz.event.AmcEvent;
import cn.icanci.loopstack.amc.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.amc.common.enums.ModuleTypeEnum;

import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * 日志事件监听
 * 
 * @author icanci
 * @since 1.0 Created in 2022/12/24 20:39
 */
@Component
public class LogEvent extends AmcEvent {

    private String              targetId;

    private ModuleTypeEnum      moduleType;

    private String              context;

    private LogOperatorTypeEnum logOperatorType;

    private Date                createTime;

    public LogEvent() {
    }

    public LogEvent(String targetId, ModuleTypeEnum moduleType, String context, LogOperatorTypeEnum logOperatorType) {
        this.targetId = targetId;
        this.moduleType = moduleType;
        this.context = context;
        this.logOperatorType = logOperatorType;
        this.createTime = new Date();
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public ModuleTypeEnum getModuleType() {
        return moduleType;
    }

    public void setModuleType(ModuleTypeEnum moduleType) {
        this.moduleType = moduleType;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public LogOperatorTypeEnum getLogOperatorType() {
        return logOperatorType;
    }

    public void setLogOperatorType(LogOperatorTypeEnum logOperatorType) {
        this.logOperatorType = logOperatorType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
