package cn.icanci.loopstack.amc.biz.service.impl;

import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.LogOperateDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.LogOperateDO;
import cn.icanci.loopstack.amc.biz.mapper.LogOperateMapper;
import cn.icanci.loopstack.amc.biz.service.LogService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.log.LogOperateVO;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 09:54
 */
@Service
public class LogServiceImpl implements LogService {
    @Resource
    private LogOperateDAO    logOperateDAO;

    @Resource
    private LogOperateMapper logOperateMapper;

    @Override
    public void log(LogOperateVO logOperate) {
        logOperateDAO.insert(logOperateMapper.vo2do(logOperate));
    }

    @Override
    public PageList<LogOperateVO> queryPage(String module, String targetId, int pageNum, int pageSize) {
        LogOperateDO operate = new LogOperateDO();
        operate.setModule(module);
        operate.setTargetId(targetId);
        PageList<LogOperateDO> pageQuery = logOperateDAO.pageQuery(operate, pageNum, pageSize);
        return new PageList<>(logOperateMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }
}
