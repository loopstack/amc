package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.log.LogOperateVO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/07 23:31
 */
public interface LogService {
    /**
     * 记录日志
     *
     * @param logOperate logOperate
     */
    void log(LogOperateVO logOperate);

    /**
     * queryPage
     *
     * @param module module
     * @param targetId targetId
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return PageList<LogOperateVO>
     */
    PageList<LogOperateVO> queryPage(String module, String targetId, int pageNum, int pageSize);
}
