package cn.icanci.loopstack.amc.biz.mapper.converter;

import cn.icanci.loopstack.script.enums.ScriptTypeEnum;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:16
 */
@Component
public class ScriptTypeEnumConverter extends AbstractBaseConverter<ScriptTypeEnum> {
}
