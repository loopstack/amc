package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.report.AppMockCallReportVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/18 11:45
 */
public interface AppMockCallReportService {

    /**
     * 分页查询
     * 
     * @param vo 查询模型
     * @param currentPage 当前页
     * @param pageSize 页大小
     * @return 返回查询模型
     */
    PageList<AppMockCallReportVO> queryPage(AppMockCallReportVO vo, int currentPage, int pageSize);

    /**
     * 查询所有数据
     * 
     * @param vo vo
     * @return 返回查询的数据
     */
    List<AppMockCallReportVO> queryAll(AppMockCallReportVO vo);

    /**
     * 根据id删除
     *
     * @param id id
     */
    void deleteById(String id);
}
