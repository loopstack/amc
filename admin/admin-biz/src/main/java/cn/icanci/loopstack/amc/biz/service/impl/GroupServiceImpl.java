package cn.icanci.loopstack.amc.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.GroupDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.GroupDO;
import cn.icanci.loopstack.amc.biz.event.log.LogEvent;
import cn.icanci.loopstack.amc.biz.mapper.GroupMapper;
import cn.icanci.loopstack.amc.biz.service.BaseService;
import cn.icanci.loopstack.amc.biz.service.GroupService;
import cn.icanci.loopstack.amc.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.amc.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.GroupVO;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 14:00
 */
@Service
public class GroupServiceImpl extends BaseService<GroupVO> implements GroupService {
    @Resource
    private GroupDAO    groupDAO;
    @Resource
    private GroupMapper groupMapper;

    @Override
    public PageList<GroupVO> queryPage(GroupVO web2vo, int currentPage, int pageSize) {
        PageList<GroupDO> pageQuery = groupDAO.pageQuery(groupMapper.vo2do(web2vo), currentPage, pageSize);
        return new PageList<>(groupMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public void save(GroupVO groupVO) {
        if (doInsert(groupVO)) {
            GroupDO groupDO = groupMapper.vo2do(groupVO);
            groupDAO.insert(groupDO);
            eventDispatcher.fire(new LogEvent(groupDO.getUuid(), ModuleTypeEnum.AMC_GROUP, JSONUtil.toJsonStr(groupDO), LogOperatorTypeEnum.CREATE));
        } else {
            groupDAO.update(groupMapper.vo2do(groupVO));
            eventDispatcher.fire(new LogEvent(groupVO.getUuid(), ModuleTypeEnum.AMC_GROUP, JSONUtil.toJsonStr(groupVO), LogOperatorTypeEnum.UPDATE));
        }
    }

    @Override
    public GroupVO queryByGroupId(String groupId) {
        return groupMapper.do2vo(groupDAO.queryByGroupId(groupId));
    }

    @Override
    public GroupVO queryByGroupName(String groupName) {
        return groupMapper.do2vo(groupDAO.queryByGroupName(groupName));
    }

    @Override
    public List<TextValue> loadSelector() {
        List<GroupDO> groupList = groupDAO.queryAll();
        if (CollectionUtils.isEmpty(groupList)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (GroupDO groupDO : groupList) {
            String label;
            GroupVO groupVO = groupMapper.do2vo(groupDO);
            if (isDeleted(groupVO)) {
                label = String.format(DELETED_FORMAT, groupVO.getGroupName());
            } else {
                label = String.format(NOT_DELETED_FORMAT, groupVO.getGroupName());
            }
            textValues.add(new TextValue(label, groupVO.getUuid()));
        }
        return textValues;
    }
}
