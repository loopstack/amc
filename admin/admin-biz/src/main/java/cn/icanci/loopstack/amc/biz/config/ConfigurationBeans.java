package cn.icanci.loopstack.amc.biz.config;

import cn.icanci.loopstack.api.client.Client;
import cn.icanci.loopstack.api.client.http.HttpClientImpl;
import cn.icanci.loopstack.lsi.event.DefaultEventDispatcher;
import cn.icanci.loopstack.lsi.event.EventDispatcher;
import cn.icanci.loopstack.script.LsiScriptEngine;
import cn.icanci.loopstack.script.LsiScriptEngineManager;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author icanci(1205068)
 * @version Id: Config, v 0.1 2022/12/24 20:22 icanci Exp $
 */
@Configuration
public class ConfigurationBeans {
    /**
     * 事件分发器
     * 
     * @return 返回事件分发器
     */
    @Bean(name = "eventDispatcher")
    public EventDispatcher eventDispatcher() {
        return new DefaultEventDispatcher();
    }

    /**
     * http client
     *
     * @return HttpClient
     */
    @Bean(name = "httpClient")
    public Client httpClient() {
        return HttpClientImpl.getInstance();
    }

    /**
     * 脚本执行引擎
     *
     * @return 脚本执行引擎
     */
    @Bean(name = "lsiScriptEngine")
    public LsiScriptEngine lsiScriptEngine() {
        return LsiScriptEngineManager.getLsiScriptEngine();
    }
}