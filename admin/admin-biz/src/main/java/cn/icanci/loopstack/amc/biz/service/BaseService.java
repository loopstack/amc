package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.admin.dal.utils.EnvUtils;
import cn.icanci.loopstack.amc.admin.dal.utils.IDHolder;
import cn.icanci.loopstack.amc.common.model.config.BaseVO;
import cn.icanci.loopstack.lsi.event.EventDispatcher;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/07 18:39
 */
public abstract class BaseService<T extends BaseVO> {
    /** label 格式化 */
    protected static final String DELETED_FORMAT     = "[无效] %s";

    /** label 格式化 */
    protected static final String NOT_DELETED_FORMAT = "[有效] %s";

    protected String getEnv() {
        return EnvUtils.getEnv();
    }

    private String genUuid() {
        return IDHolder.generateNoBySnowFlake("CFG");
    }

    @Resource
    protected EventDispatcher eventDispatcher;

    /**
     * 是否是插入
     *
     * @param t t
     * @return 返回是否是插入
     */
    protected boolean doInsert(T t) {
        return StringUtils.isBlank(t.getId());
    }

    /**
     * 是否是删除的
     *
     * @param t t
     * @return 返回是否是删除的
     */
    protected boolean isDeleted(T t) {
        return t.getIsDelete() == 1;
    }

}
