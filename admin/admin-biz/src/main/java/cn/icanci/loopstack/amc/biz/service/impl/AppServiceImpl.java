package cn.icanci.loopstack.amc.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.AppDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppDO;
import cn.icanci.loopstack.amc.biz.event.log.LogEvent;
import cn.icanci.loopstack.amc.biz.mapper.AppMapper;
import cn.icanci.loopstack.amc.biz.service.AppService;
import cn.icanci.loopstack.amc.biz.service.BaseService;
import cn.icanci.loopstack.amc.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.amc.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppVO;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:55
 */
@Service
public class AppServiceImpl extends BaseService<AppVO> implements AppService {

    @Resource
    private AppDAO    appDAO;
    @Resource
    private AppMapper appMapper;

    @Override
    public PageList<AppVO> queryPage(AppVO web2vo, int currentPage, int pageSize) {
        PageList<AppDO> pageQuery = appDAO.pageQuery(appMapper.vo2do(web2vo), currentPage, pageSize);
        return new PageList<>(appMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public void save(AppVO app) {
        if (doInsert(app)) {
            AppDO appDO = appMapper.vo2do(app);
            appDAO.insert(appDO);
            eventDispatcher.fire(new LogEvent(appDO.getUuid(), ModuleTypeEnum.AMC_APP, JSONUtil.toJsonStr(appDO), LogOperatorTypeEnum.CREATE));
        } else {
            appDAO.update(appMapper.vo2do(app));
            eventDispatcher.fire(new LogEvent(app.getUuid(), ModuleTypeEnum.AMC_APP, JSONUtil.toJsonStr(app), LogOperatorTypeEnum.UPDATE));
        }
    }

    @Override
    public AppVO queryByAppId(String appId) {
        return appMapper.do2vo(appDAO.queryByAppId(appId));
    }

    @Override
    public AppVO queryByAppName(String appName) {
        return appMapper.do2vo(appDAO.queryByAppName(appName));
    }

    @Override
    public List<TextValue> loadSelector() {
        List<AppDO> appList = appDAO.queryAll();
        if (CollectionUtils.isEmpty(appList)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (AppDO appDO : appList) {
            String label;
            AppVO appVO = appMapper.do2vo(appDO);
            if (isDeleted(appVO)) {
                label = String.format(DELETED_FORMAT, appVO.getAppName());
            } else {
                label = String.format(NOT_DELETED_FORMAT, appVO.getAppName());
            }
            textValues.add(new TextValue(label, appVO.getUuid()));
        }
        return textValues;
    }

    @Override
    public List<TextValue> loadAppIdSelector() {
        List<AppDO> appList = appDAO.queryAll();
        if (CollectionUtils.isEmpty(appList)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (AppDO appDO : appList) {
            String label;
            AppVO appVO = appMapper.do2vo(appDO);
            if (isDeleted(appVO)) {
                label = String.format(DELETED_FORMAT, appVO.getAppName());
            } else {
                label = String.format(NOT_DELETED_FORMAT, appVO.getAppName());
            }
            textValues.add(new TextValue(label, appVO.getAppId()));
        }
        return textValues;
    }
}
