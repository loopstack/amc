package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:04
 */
public interface AppService {
    PageList<AppVO> queryPage(AppVO web2vo, int currentPage, int pageSize);

    void save(AppVO web2vo);

    AppVO queryByAppId(String appId);

    AppVO queryByAppName(String appName);

    List<TextValue> loadSelector();

    List<TextValue> loadAppIdSelector();
}
