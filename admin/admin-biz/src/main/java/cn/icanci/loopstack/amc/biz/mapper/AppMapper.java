package cn.icanci.loopstack.amc.biz.mapper;

import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppDO;
import cn.icanci.loopstack.amc.common.model.config.AppVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:45
 */
@Mapper(componentModel = "spring", uses = {}, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface AppMapper extends BaseMapper<AppDO, AppVO> {
}
