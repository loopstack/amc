package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.TeamVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:07
 */
public interface TeamService {
    PageList<TeamVO> queryPage(TeamVO web2vo, int currentPage, int pageSize);

    void save(TeamVO web2vo);

    TeamVO queryByTeamId(String teamId);

    TeamVO queryByTeamName(String teamName);

    List<TextValue> loadSelector();
}
