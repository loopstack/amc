package cn.icanci.loopstack.amc.biz.event.log;

import cn.icanci.loopstack.amc.admin.dal.utils.EnvUtils;
import cn.icanci.loopstack.amc.biz.service.LogService;
import cn.icanci.loopstack.amc.common.model.log.LogOperateVO;
import cn.icanci.loopstack.lsi.event.BaseEventListener;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * 日志事件监听
 * - 日志表进行日志的记录操作
 * 
 * @author icanci
 * @since 1.0 Created in 2022/12/24 20:40
 */
@Service
public class LogEventListener extends BaseEventListener<LogEvent> {

    @Resource
    private LogService logService;

    @Override
    protected void event(LogEvent event) {
        LogOperateVO logOperateVO = new LogOperateVO();
        logOperateVO.setModule(event.getModuleType());
        logOperateVO.setTargetId(event.getTargetId());
        logOperateVO.setOperatorType(event.getLogOperatorType());
        logOperateVO.setContent(event.getContext());
        logOperateVO.setCreateTime(event.getCreateTime());
        logOperateVO.setEnv(EnvUtils.getEnv());

        logService.log(logOperateVO);
    }
}
