package cn.icanci.loopstack.amc.biz.service;

import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.GroupVO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:06
 */
public interface GroupService {
    PageList<GroupVO> queryPage(GroupVO web2vo, int currentPage, int pageSize);

    void save(GroupVO web2vo);

    GroupVO queryByGroupId(String groupId);

    GroupVO queryByGroupName(String groupName);

    List<TextValue> loadSelector();
}
