package cn.icanci.loopstack.amc.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.TeamDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.TeamDO;
import cn.icanci.loopstack.amc.biz.event.log.LogEvent;
import cn.icanci.loopstack.amc.biz.mapper.TeamMapper;
import cn.icanci.loopstack.amc.biz.service.BaseService;
import cn.icanci.loopstack.amc.biz.service.TeamService;
import cn.icanci.loopstack.amc.common.enums.LogOperatorTypeEnum;
import cn.icanci.loopstack.amc.common.enums.ModuleTypeEnum;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.TeamVO;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 19:46
 */
@Service
public class TeamServiceImpl extends BaseService<TeamVO> implements TeamService {
    @Resource
    private TeamDAO    teamDAO;
    @Resource
    private TeamMapper teamMapper;

    @Override
    public PageList<TeamVO> queryPage(TeamVO web2vo, int currentPage, int pageSize) {
        PageList<TeamDO> pageQuery = teamDAO.pageQuery(teamMapper.vo2do(web2vo), currentPage, pageSize);
        return new PageList<>(teamMapper.dos2vos(pageQuery.getData()), pageQuery.getPaginator());
    }

    @Override
    public void save(TeamVO team) {
        if (doInsert(team)) {
            TeamDO teamDO = teamMapper.vo2do(team);
            teamDAO.insert(teamDO);
            eventDispatcher.fire(new LogEvent(teamDO.getUuid(), ModuleTypeEnum.AMC_TEAM, JSONUtil.toJsonStr(teamDO), LogOperatorTypeEnum.CREATE));
        } else {
            teamDAO.update(teamMapper.vo2do(team));
            eventDispatcher.fire(new LogEvent(team.getUuid(), ModuleTypeEnum.AMC_TEAM, JSONUtil.toJsonStr(team), LogOperatorTypeEnum.UPDATE));
        }
    }

    @Override
    public TeamVO queryByTeamId(String teamId) {
        return teamMapper.do2vo(teamDAO.queryByTeamId(teamId));
    }

    @Override
    public TeamVO queryByTeamName(String teamName) {
        return teamMapper.do2vo(teamDAO.queryByTeamName(teamName));
    }

    @Override
    public List<TextValue> loadSelector() {
        List<TeamDO> teamList = teamDAO.queryAll();
        if (CollectionUtils.isEmpty(teamList)) {
            return Lists.newArrayList();
        }
        List<TextValue> textValues = Lists.newArrayList();
        for (TeamDO teamDO : teamList) {
            String label;
            TeamVO teamVO = teamMapper.do2vo(teamDO);
            if (isDeleted(teamVO)) {
                label = String.format(DELETED_FORMAT, teamVO.getTeamName());
            } else {
                label = String.format(NOT_DELETED_FORMAT, teamVO.getTeamName());
            }
            textValues.add(new TextValue(label, teamVO.getUuid()));
        }
        return textValues;
    }
}
