package cn.icanci.loopstack.amc.biz.mapper;

import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.LogOperateDO;
import cn.icanci.loopstack.amc.biz.mapper.converter.ModuleTypeEnumConverter;
import cn.icanci.loopstack.amc.common.model.log.LogOperateVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 14:19
 */
@Mapper(componentModel = "spring", uses = { ModuleTypeEnumConverter.class }, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface LogOperateMapper extends BaseMapper<LogOperateDO, LogOperateVO> {
}
