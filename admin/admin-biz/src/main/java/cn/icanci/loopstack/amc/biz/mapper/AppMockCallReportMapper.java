package cn.icanci.loopstack.amc.biz.mapper;

import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallReportDO;
import cn.icanci.loopstack.amc.common.model.report.AppMockCallReportVO;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/18 11:50
 */
@Mapper(componentModel = "spring", uses = {}, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface AppMockCallReportMapper extends BaseMapper<AppMockCallReportDO, AppMockCallReportVO> {
}
