package cn.icanci.loopstack.amc.admin.views;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/14 19:47
 */
@ComponentScan(basePackages = { "cn.icanci.loopstack.amc" })
@SpringBootApplication
public class AdminViewApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminViewApplication.class, args);
    }
}
