import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss' // global css
import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon
import '@/permission'
import moment from "moment";

// 英文版本
// Vue.use(ElementUI, { locale })
// 中文版
Vue.use(ElementUI)
Vue.config.productionTip = false
// 自定义全局方法
Vue.prototype.$cloneObj = function (data) {
  return JSON.parse(JSON.stringify(data))
}
Vue.prototype.vueDateFormat = function (date) {
  return moment(date).format('YYYY-MM-DD HH:mm:ss');
}

if (!String.prototype.format) {
  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
        ;
    });
  };
}

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
