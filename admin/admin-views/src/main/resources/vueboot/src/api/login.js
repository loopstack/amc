import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/amc/user/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/amc/user/info/' + token,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/amc/user/logout',
    method: 'post'
  })
}
