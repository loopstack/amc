import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param group group
 * @param paginator paginator
 * @returns {*}
 */
export async function groupQuery(group, paginator) {
  return await request({
    url: '/amc/group/query',
    method: 'post',
    data: {
      'group': group,
      'paginator': paginator
    }
  })
}

/**
 * 保存
 *
 * @param group group
 * @returns {*}
 */
export async function saveGroup(group) {
  return await request({
    url: '/amc/group/save',
    method: 'post',
    data: group
  })
}

/**
 * remoteValidateGroupId
 *
 * @param groupId groupId
 * @returns {*}
 */
export async function remoteValidateGroupId(groupId) {
  return await request({
    url: '/amc/group/validateGroupId/' + groupId,
    method: 'get'
  })
}

/**
 * remoteValidateGroupName
 *
 * @param groupName groupName
 * @returns {*}
 */
export async function remoteValidateGroupName(groupName) {
  return await request({
    url: '/amc/group/validateGroupName/' + groupName,
    method: 'get'
  })
}
