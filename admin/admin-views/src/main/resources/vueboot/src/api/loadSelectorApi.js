import request from '@/utils/request'

/**
 * loadGroupSelector
 *
 * @returns {*}
 */
export async function loadGroupSelector() {
  let ret = await request({
    url: '/amc/group/loadSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadTeamSelector
 *
 * @returns {*}
 */
export async function loadTeamSelector() {
  let ret = await request({
    url: '/amc/team/loadSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadAppSelector
 *
 * @returns {*}
 */
export async function loadAppSelector() {
  let ret = await request({
    url: '/amc/app/loadSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadAppSelector
 *
 * @returns {*}
 */
export async function loadAppIdSelector() {
  let ret = await request({
    url: '/amc/app/loadAppIdSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadAppMockCallSelector
 *
 * @returns {*}
 */
export async function loadAppMockCallSelector() {
  let ret = await request({
    url: '/amc/appMockCall/loadSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadAppMockCallTestSelector
 *
 * @returns {*}
 */
export async function loadAppMockCallTestSelector() {
  let ret = await request({
    url: '/amc/appMockCallTest/loadSelector',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}

/**
 * loadScriptSelector
 *
 * @returns {*}
 */
export async function loadScriptSelector() {
  let ret = await request({
    url: '/amc/common/scriptType',
    method: 'get',
  });
  if (ret.ok) {
    return ret.data.textValues;
  }
  return []
}
