import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param logOperate logOperate
 * @param paginator paginator
 * @returns {*}
 */
export async function logPageQuery(logOperate, paginator) {
  return await request({
    url: '/amc/log/query',
    method: 'post',
    data: {
      'logOperate': logOperate,
      'paginator': paginator
    }
  })
}
