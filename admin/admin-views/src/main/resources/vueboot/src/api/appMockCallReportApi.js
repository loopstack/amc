import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param appMockCallReport appMockCallReport
 * @param paginator paginator
 * @returns {*}
 */
export async function appMockCallReportQuery(appMockCallReport, paginator) {
  return await request({
    url: '/amc/appMockCallReport/query',
    method: 'post',
    data: {
      'appMockCallReport': appMockCallReport,
      'paginator': paginator
    }
  })
}

/**
 * appMockCallReportDownload
 *
 * @param appMockCallReport appMockCallReport
 * @returns {*}
 */
export async function appMockCallReportDownload(appMockCallReport) {
  return await request({
    url: '/amc/appMockCallReport/download',
    method: 'post',
    data: appMockCallReport,
    responseType: 'blob'
  })
}

/**
 * appMockCallReportDelete
 *
 * @param id id
 * @returns {*}
 */
export async function appMockCallReportDelete(id) {
  return await request({
    url: '/amc/appMockCallReport/delete/' + id,
    method: 'get',
  })
}
