import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param appMockCallTest appMockCallTest
 * @param paginator paginator
 * @returns {*}
 */
export async function appMockCallTestQuery(appMockCallTest, paginator) {
  return await request({
    url: '/amc/appMockCallTest/query',
    method: 'post',
    data: {
      'appMockCallTest': appMockCallTest,
      'paginator': paginator
    }
  })
}

/**
 * 保存
 *
 * @param appMockCallTest appMockCallTest
 * @returns {*}
 */
export async function saveAppMockTestCall(appMockCallTest) {
  return await request({
    url: '/amc/appMockCallTest/save',
    method: 'post',
    data: appMockCallTest
  })
}

/**
 * validateAppMockCallName
 *
 * @param mockTestName mockTestName
 * @returns {*}
 */
export async function validateAppMockCallTestName(mockTestName) {
  return await request({
    url: '/amc/appMockCallTest/validateAppMockCallTestName/' + mockTestName,
    method: 'get'
  })
}
