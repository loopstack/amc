import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param appMockCall appMockCall
 * @param paginator paginator
 * @returns {*}
 */
export async function appMockCallQuery(appMockCall, paginator) {
  return await request({
    url: '/amc/appMockCall/query',
    method: 'post',
    data: {
      'appMockCall': appMockCall,
      'paginator': paginator
    }
  })
}

/**
 * 保存
 *
 * @param appMockCall appMockCall
 * @returns {*}
 */
export async function saveAppMockCall(appMockCall) {
  return await request({
    url: '/amc/appMockCall/save',
    method: 'post',
    data: appMockCall
  })
}

/**
 * validateAppMockCallName
 *
 * @param mockName mockName
 * @returns {*}
 */
export async function validateAppMockCallName(mockName) {
  return await request({
    url: '/amc/appMockCall/validateAppMockCallName/' + mockName,
    method: 'get'
  })
}

/**
 * publishMockCall
 *
 * @param uuid uuid
 * @returns {*}
 */
export async function publishMockCall(uuid) {
  return await request({
    url: '/amc/appMockCall/publishMockCall/' + uuid,
    method: 'get'
  })
}

/**
 * 测试
 *
 * @param appMockCall appMockCall
 * @param scriptTest scriptTest
 * @returns {*}
 */
export async function mockCallDebug(appMockCall, scriptTest) {
  return await request({
    url: '/amc/appMockCall/debug',
    method: 'post',
    data: {
      'appMockCall': appMockCall,
      'scriptTest': scriptTest
    }
  })
}
