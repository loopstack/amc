import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param app app
 * @param paginator paginator
 * @returns {*}
 */
export async function appQuery(app, paginator) {
  return await request({
    url: '/amc/app/query',
    method: 'post',
    data: {
      'app': app,
      'paginator': paginator
    }
  })
}

/**
 * 保存
 *
 * @param app app
 * @returns {*}
 */
export async function saveApp(app) {
  return await request({
    url: '/amc/app/save',
    method: 'post',
    data: app
  })
}

/**
 * remoteValidateAppId
 *
 * @param appId appId
 * @returns {*}
 */
export async function remoteValidateAppId(appId) {
  return await request({
    url: '/amc/app/validateAppId/' + appId,
    method: 'get'
  })
}

/**
 * remoteValidateAppName
 *
 * @param appName appName
 * @returns {*}
 */
export async function remoteValidateAppName(appName) {
  return await request({
    url: '/amc/app/validateAppName/' + appName,
    method: 'get'
  })
}
