import request from '@/utils/request'

/**
 * 分页查询
 *
 * @param team team
 * @param paginator paginator
 * @returns {*}
 */
export async function teamQuery(team, paginator) {
  return await request({
    url: '/amc/team/query',
    method: 'post',
    data: {
      'team': team,
      'paginator': paginator
    }
  })
}

/**
 * 保存
 *
 * @param team team
 * @returns {*}
 */
export async function saveTeam(team) {
  return await request({
    url: '/amc/team/save',
    method: 'post',
    data: team
  })
}

/**
 * remoteValidateTeamId
 *
 * @param teamId teamId
 * @returns {*}
 */
export async function remoteValidateTeamId(teamId) {
  return await request({
    url: '/amc/team/validateTeamId/' + teamId,
    method: 'get'
  })
}

/**
 * remoteValidateTeamName
 *
 * @param teamName teamName
 * @returns {*}
 */
export async function remoteValidateTeamName(teamName) {
  return await request({
    url: '/amc/team/validateTeamName/' + teamName,
    method: 'get'
  })
}
