package cn.icanci.loopstack.amc.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.AppMockCallTestDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallTestDO;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 19:19
 */
@Service("appMockCallTestDAO")
public class MongoAppMockCallTestDAO extends AbstractBaseDAO<AppMockCallTestDO> implements AppMockCallTestDAO {

    @Override
    public void insert(AppMockCallTestDO appMockCallTestDO) {
        super.insert(appMockCallTestDO);
        mongoTemplate.insert(appMockCallTestDO, COLLECTION_NAME);
    }

    @Override
    public void update(AppMockCallTestDO appMockCallTestDO) {
        super.update(appMockCallTestDO);
        mongoTemplate.save(appMockCallTestDO, COLLECTION_NAME);
    }

    @Override
    public List<AppMockCallTestDO> queryAll() {
        Criteria criteria = Criteria.where(AppMockCallTestColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<AppMockCallTestDO> pageQuery(AppMockCallTestDO appMockCallTestDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(AppMockCallTestColumn.env).is(DEFAULT_ENV);

        if (StringUtils.isNotBlank(appMockCallTestDO.getMockTestName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(AppMockCallTestColumn.mockTestName).regex("^.*" + appMockCallTestDO.getMockTestName() + ".*$", "i");
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, AppMockCallTestColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public AppMockCallTestDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(AppMockCallTestColumn._id).is(_id);
        criteria.and(AppMockCallTestColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public AppMockCallTestDO queryByAppMockName(String mockName) {
        Criteria criteria = Criteria.where(AppMockCallTestColumn.mockTestName).is(mockName);
        criteria.and(AppMockCallTestColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

}