package cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallTestDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:23
 */
public interface AppMockCallTestDAO extends BaseDAO<AppMockCallTestDO> {
    /**
     * 文档对应的名字
     */
    String                   COLLECTION_NAME  = "amc-app-mock-call-test";
    /**
     * 文档对应的Class
     */
    Class<AppMockCallTestDO> COLLECTION_CLASS = AppMockCallTestDO.class;

    AppMockCallTestDO queryByAppMockName(String mockName);

    interface AppMockCallTestColumn extends BaseColumn {
        String mockTestName = "mockTestName";
        String appUuid      = "appUuid";
        String testJson     = "testJson";
    }
}
