package cn.icanci.loopstack.amc.admin.dal.mongodb.mongo;

import cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface.AppMockCallReportDAO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallReportDO;
import cn.icanci.loopstack.amc.admin.dal.mongodb.exception.NotSupportAccessException;
import cn.icanci.loopstack.amc.common.enums.BooleanEnum;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 23:35
 */
@Service("appMockCallReportDAO")
public class MongoAppMockCallReportDAO extends AbstractBaseDAO<AppMockCallReportDO> implements AppMockCallReportDAO {

    @Override
    public void insert(AppMockCallReportDO appMockCallReportDO) {
        throw new NotSupportAccessException();
    }

    @Override
    public void update(AppMockCallReportDO appMockCallReportDO) {
        super.update(appMockCallReportDO);
        mongoTemplate.save(appMockCallReportDO, COLLECTION_NAME);
    }

    @Override
    public List<AppMockCallReportDO> queryAll() {
        Criteria criteria = Criteria.where(AppMockCallReportColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<AppMockCallReportDO> pageQuery(AppMockCallReportDO appMockCallReportDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(AppMockCallReportColumn.env).is(DEFAULT_ENV);

        if (StringUtils.isNotBlank(appMockCallReportDO.getMockName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(AppMockCallReportColumn.mockName).regex("^.*" + appMockCallReportDO.getMockName() + ".*$", "i");
        }
        if (StringUtils.isNotBlank(appMockCallReportDO.getMockUuid())) {
            criteria.and(AppMockCallReportColumn.mockUuid).is(appMockCallReportDO.getMockUuid());
        }

        criteria.and(AppMockCallReportColumn.isDelete).is(BooleanEnum.NO.getCode());
        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, AppMockCallReportColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public AppMockCallReportDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(AppMockCallReportColumn._id).is(_id);
        criteria.and(AppMockCallReportColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public List<AppMockCallReportDO> queryAllByReport(AppMockCallReportDO report) {
        Criteria criteria = Criteria.where(AppMockCallReportColumn.env).is(DEFAULT_ENV);

        if (StringUtils.isNotBlank(report.getMockName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(AppMockCallReportColumn.mockName).regex("^.*" + report.getMockName() + ".*$", "i");
        }
        if (StringUtils.isNotBlank(report.getMockUuid())) {
            criteria.and(AppMockCallReportColumn.mockUuid).is(report.getMockUuid());
        }
        criteria.and(AppMockCallReportColumn.isDelete).is(BooleanEnum.NO.getCode());

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, AppMockCallReportColumn.createTime));

        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
