package cn.icanci.loopstack.amc.admin.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.admin.dal.mongodb.dataobject.AppMockCallReportDO;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 23:28
 */
public interface AppMockCallReportDAO extends BaseDAO<AppMockCallReportDO> {
    /**
     * 文档对应的名字
     */
    String                     COLLECTION_NAME  = "amc-app-mock-call-report";
    /**
     * 文档对应的Class
     */
    Class<AppMockCallReportDO> COLLECTION_CLASS = AppMockCallReportDO.class;

    List<AppMockCallReportDO> queryAllByReport(AppMockCallReportDO report);

    interface AppMockCallReportColumn extends BaseColumn {
        /**
         * mock 过程是否成功
         */
        String success          = "success";
        /**
         * mockName
         */
        String mockName         = "mockName";
        /**
         * mockUuid
         */
        String mockUuid         = "mockUuid";
        /**
         * mock执行的请求
         */
        String mockRequest      = "mockRequest";
        /**
         * mock执行的异常信息
         */
        String mockErrorMessage = "mockErrorMessage";
        /**
         * 真正mock返回的结果
         */
        String mockResponse     = "mockResponse";
    }
}
