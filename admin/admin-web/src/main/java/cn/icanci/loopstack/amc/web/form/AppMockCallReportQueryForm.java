package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.AppMockCallReport;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/18 11:52
 */
public class AppMockCallReportQueryForm extends BaseQueryForm {

    private AppMockCallReport appMockCallReport;

    public AppMockCallReport getAppMockCallReport() {
        return appMockCallReport;
    }

    public void setAppMockCallReport(AppMockCallReport appMockCallReport) {
        this.appMockCallReport = appMockCallReport;
    }
}
