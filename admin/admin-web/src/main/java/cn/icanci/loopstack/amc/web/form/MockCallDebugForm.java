package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.AppMockCall;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 11:57
 */
public class MockCallDebugForm implements Serializable {
    private AppMockCall appMockCall;
    private String      scriptTest;

    public AppMockCall getAppMockCall() {
        return appMockCall;
    }

    public void setAppMockCall(AppMockCall appMockCall) {
        this.appMockCall = appMockCall;
    }

    public String getScriptTest() {
        return scriptTest;
    }

    public void setScriptTest(String scriptTest) {
        this.scriptTest = scriptTest;
    }
}
