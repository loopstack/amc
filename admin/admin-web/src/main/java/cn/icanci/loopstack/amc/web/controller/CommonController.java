package cn.icanci.loopstack.amc.web.controller;

import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.script.enums.ScriptTypeEnum;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 20:12
 */
@RestController
@RequestMapping("/amc/common")
public class CommonController {
    @GetMapping("scriptType")
    public R scriptType() {
        List<TextValue> textValues = Arrays.stream(ScriptTypeEnum.values()).map(x -> new TextValue(x.getDesc(), x.getCode())).collect(Collectors.toList());
        return R.builderOk().data("textValues", textValues).build();
    }

}
