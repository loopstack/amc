package cn.icanci.loopstack.amc.web.controller;

import cn.icanci.loopstack.amc.biz.service.LogService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.Paginator;
import cn.icanci.loopstack.amc.common.model.log.LogOperateVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.LogQueryForm;
import cn.icanci.loopstack.amc.web.mapper.LogOperateWebMapper;
import cn.icanci.loopstack.amc.web.model.LogOperate;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 14:15
 */
@RestController
@RequestMapping("/amc/log")
public class LogController {

    @Resource
    private LogService          logService;
    @Resource
    private LogOperateWebMapper logOperateWebMapper;

    @PostMapping("/query")
    public R query(@RequestBody LogQueryForm form) {
        LogOperate logOperate = form.getLogOperate();
        Paginator paginator = form.getPaginator();
        PageList<LogOperateVO> pageList = logService.queryPage(logOperate.getModule(), logOperate.getTargetId(), paginator.getCurrentPage(), paginator.getPageSize());
        PageList<LogOperate> ret = new PageList<>(logOperateWebMapper.vos2webs(pageList.getData()), pageList.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }
}
