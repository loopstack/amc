package cn.icanci.loopstack.amc.web.model;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 19:24
 */
public class AppMockCallTest extends Base {
    /**
     * mock 配置名称
     */
    private String mockTestName;
    /**
     * 脚本关联的uuid
     */
    private String appUuid;
    /**
     * 脚本的请求方法
     */
    private String testJson;

    public String getMockTestName() {
        return mockTestName;
    }

    public void setMockTestName(String mockTestName) {
        this.mockTestName = mockTestName;
    }

    public String getAppUuid() {
        return appUuid;
    }

    public void setAppUuid(String appUuid) {
        this.appUuid = appUuid;
    }

    public String getTestJson() {
        return testJson;
    }

    public void setTestJson(String testJson) {
        this.testJson = testJson;
    }
}
