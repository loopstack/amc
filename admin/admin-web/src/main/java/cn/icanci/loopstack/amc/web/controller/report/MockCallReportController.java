package cn.icanci.loopstack.amc.web.controller.report;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.icanci.loopstack.amc.biz.service.AppMockCallReportService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.report.AppMockCallReportVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.AppMockCallReportQueryForm;
import cn.icanci.loopstack.amc.web.mapper.AppMockCallReportWebMapper;
import cn.icanci.loopstack.amc.web.model.AppMockCallReport;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/18 11:44
 */
@Controller
@RequestMapping("/amc/appMockCallReport")
public class MockCallReportController {
    private static final Logger        logger = LoggerFactory.getLogger(MockCallReportController.class);
    @Resource
    private AppMockCallReportService   appMockCallReportService;

    @Resource
    private AppMockCallReportWebMapper appMockCallReportWebMapper;

    @PostMapping("query")
    @ResponseBody
    public R query(@RequestBody AppMockCallReportQueryForm form) {
        PageList<AppMockCallReportVO> queryPage = appMockCallReportService.queryPage(appMockCallReportWebMapper.web2vo(form.getAppMockCallReport()),
            form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize());
        PageList<AppMockCallReport> ret = new PageList<>(appMockCallReportWebMapper.vos2webs(queryPage.getData()), queryPage.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }

    @RequestMapping("download")
    public void download(AppMockCallReport appMockCallReport, HttpServletResponse response) {
        List<AppMockCallReportVO> ret = appMockCallReportService.queryAll(appMockCallReportWebMapper.web2vo(appMockCallReport));
        excelExport(response, ret);
    }

    @GetMapping("delete/{id}")
    @ResponseBody
    public R deleteById(@PathVariable("id") String id) {
        appMockCallReportService.deleteById(id);
        return R.builderOk().build();
    }

    /**
     * 导出excel
     *
     * @param <T>      泛型
     * @param response 响应
     * @param list     数据集
     */
    protected <T> void excelExport(HttpServletResponse response, List<T> list) {
        try {
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            WriteFont headWriteFont = new WriteFont();
            headWriteFont.setFontHeightInPoints((short) 11);
            headWriteCellStyle.setWriteFont(headWriteFont);
            HorizontalCellStyleStrategy horizontalCellStyleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition",
                "attachment;filename=" + URLEncoder.encode("【" + DateUtil.format(new Date(), DatePattern.CHINESE_DATE_TIME_PATTERN) + "】" + "AMC-Cloud执行报告", "UTF-8") + ".xlsx");
            ExcelWriterSheetBuilder writerSheetBuilder = EasyExcel.write(response.getOutputStream()).sheet();
            writerSheetBuilder.registerWriteHandler(horizontalCellStyleStrategy);
            writerSheetBuilder.head(AppMockCallReportVO.class);
            writerSheetBuilder.sheetName("执行报告");
            writerSheetBuilder.doWrite(list);
        } catch (Exception e) {
            logger.error("[MockCallReportController][excelExport] error msg:{}", e.getLocalizedMessage());
        }
    }
}
