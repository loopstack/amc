package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.AppMockCallTest;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 19:31
 */
public class AppMockCallTestQueryForm extends BaseQueryForm {

    private AppMockCallTest appMockCallTest;

    public AppMockCallTest getAppMockCallTest() {
        return appMockCallTest;
    }

    public void setAppMockCallTest(AppMockCallTest appMockCallTest) {
        this.appMockCallTest = appMockCallTest;
    }
}
