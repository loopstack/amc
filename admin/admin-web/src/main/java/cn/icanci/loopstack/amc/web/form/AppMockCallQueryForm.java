package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.AppMockCall;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/08 10:28
 */
public class AppMockCallQueryForm extends BaseQueryForm {
    /**
     * App 查询信息
     */
    private AppMockCall appMockCall;

    public AppMockCall getAppMockCall() {
        return appMockCall;
    }

    public void setAppMockCall(AppMockCall appMockCall) {
        this.appMockCall = appMockCall;
    }
}
