package cn.icanci.loopstack.amc.web.controller.config;

import cn.icanci.loopstack.amc.biz.service.GroupService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.GroupVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.GroupQueryForm;
import cn.icanci.loopstack.amc.web.mapper.GroupWebMapper;
import cn.icanci.loopstack.amc.web.model.Group;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/07 18:17
 */
@RestController
@RequestMapping("/amc/group")
public class GroupController {
    @Resource
    private GroupService   groupService;

    @Resource
    private GroupWebMapper groupWebMapper;

    @PostMapping("query")
    public R query(@RequestBody GroupQueryForm form) {
        PageList<GroupVO> queryPage = groupService.queryPage(groupWebMapper.web2vo(form.getGroup()), form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize());
        PageList<Group> ret = new PageList<>(groupWebMapper.vos2webs(queryPage.getData()), queryPage.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }

    @PostMapping("save")
    public R save(@RequestBody Group group) {
        groupService.save(groupWebMapper.web2vo(group));
        return R.builderOk().build();
    }

    @GetMapping("validateGroupId/{groupId:.*}")
    public R validateGroupId(@PathVariable("groupId") String groupId) {
        GroupVO group = groupService.queryByGroupId(groupId);
        return R.builderOk().data("result", group == null).build();
    }

    @GetMapping("validateGroupName/{groupName:.*}")
    public R validateGroupName(@PathVariable("groupName") String groupName) {
        GroupVO group = groupService.queryByGroupName(groupName);
        return R.builderOk().data("result", group == null).build();
    }

    @GetMapping("loadSelector")
    public R loadSelector() {
        List<TextValue> textValues = groupService.loadSelector();
        return R.builderOk().data("textValues", textValues).build();
    }

}
