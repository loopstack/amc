package cn.icanci.loopstack.amc.web.controller.config;

import cn.icanci.loopstack.amc.biz.service.AppService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.AppQueryForm;
import cn.icanci.loopstack.amc.web.mapper.AppWebMapper;
import cn.icanci.loopstack.amc.web.model.App;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/06 21:11
 */
@RestController
@RequestMapping("/amc/app")
public class AppController {
    @Resource
    private AppService   appService;

    @Resource
    private AppWebMapper appWebMapper;

    @PostMapping("query")
    public R query(@RequestBody AppQueryForm form) {
        PageList<AppVO> queryPage = appService.queryPage(appWebMapper.web2vo(form.getApp()), form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize());
        PageList<App> ret = new PageList<>(appWebMapper.vos2webs(queryPage.getData()), queryPage.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }

    @PostMapping("save")
    public R save(@RequestBody App app) {
        appService.save(appWebMapper.web2vo(app));
        return R.builderOk().build();
    }

    @GetMapping("validateAppId/{appId:.*}")
    public R validateAppId(@PathVariable("appId") String appId) {
        AppVO app = appService.queryByAppId(appId);
        return R.builderOk().data("result", app == null).build();
    }

    @GetMapping("validateAppName/{appName:.*}")
    public R validateAppName(@PathVariable("appName") String appName) {
        AppVO app = appService.queryByAppName(appName);
        return R.builderOk().data("result", app == null).build();
    }

    @GetMapping("loadSelector")
    public R loadSelector() {
        List<TextValue> textValues = appService.loadSelector();
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("loadAppIdSelector")
    public R loadAppIdSelector() {
        List<TextValue> textValues = appService.loadAppIdSelector();
        return R.builderOk().data("textValues", textValues).build();
    }
}
