package cn.icanci.loopstack.amc.web.mapper;

import cn.icanci.loopstack.amc.common.model.config.TeamVO;
import cn.icanci.loopstack.amc.web.model.Team;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:55
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface TeamWebMapper extends BaseWebMapper<Team, TeamVO> {
}
