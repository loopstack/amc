package cn.icanci.loopstack.amc.web.mapper;

import cn.icanci.loopstack.amc.common.model.config.AppMockCallTestVO;
import cn.icanci.loopstack.amc.web.model.AppMockCallTest;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:55
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface AppMockCallTestWebMapper extends BaseWebMapper<AppMockCallTest, AppMockCallTestVO> {
}
