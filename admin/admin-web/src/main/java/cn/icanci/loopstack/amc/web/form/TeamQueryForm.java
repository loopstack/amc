package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.Team;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/08 09:51
 */
public class TeamQueryForm extends BaseQueryForm {
    /**
     * Team 查询信息
     */
    private Team team;

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
