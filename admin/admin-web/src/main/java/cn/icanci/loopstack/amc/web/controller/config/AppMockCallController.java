package cn.icanci.loopstack.amc.web.controller.config;

import cn.icanci.loopstack.amc.biz.model.AppMockDebugResult;
import cn.icanci.loopstack.amc.biz.service.AppMockCallService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppMockCallVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.AppMockCallQueryForm;
import cn.icanci.loopstack.amc.web.form.MockCallDebugForm;
import cn.icanci.loopstack.amc.web.mapper.AppMockCallWebMapper;
import cn.icanci.loopstack.amc.web.model.AppMockCall;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/06 21:11
 */
@RestController
@RequestMapping("/amc/appMockCall")
public class AppMockCallController {
    @Resource
    private AppMockCallService   appMockCallService;
    @Resource
    private AppMockCallWebMapper appMockCallWebMapper;

    @PostMapping("query")
    public R query(@RequestBody AppMockCallQueryForm form) {
        PageList<AppMockCallVO> queryPage = appMockCallService.queryPage(appMockCallWebMapper.web2vo(form.getAppMockCall()), form.getPaginator().getCurrentPage(),
            form.getPaginator().getPageSize());
        PageList<AppMockCall> ret = new PageList<>(appMockCallWebMapper.vos2webs(queryPage.getData()), queryPage.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }

    @PostMapping("save")
    public R save(@RequestBody AppMockCall appMock) {
        appMockCallService.save(appMockCallWebMapper.web2vo(appMock));
        return R.builderOk().build();
    }

    @GetMapping("validateAppMockCallName/{mockName:.*}")
    public R validateAppMockCallName(@PathVariable("mockName") String mockName) {
        AppMockCallVO appMock = appMockCallService.validateAppMockCallName(mockName);
        return R.builderOk().data("result", appMock == null).build();
    }

    @GetMapping("loadSelector")
    public R loadSelector() {
        List<TextValue> textValues = appMockCallService.loadSelector();
        return R.builderOk().data("textValues", textValues).build();
    }

    @GetMapping("publishMockCall/{uuid}")
    public R publishMockCall(@PathVariable("uuid") String uuid) {
        return appMockCallService.publishMockCall(uuid);
    }

    @PostMapping("debug")
    public R debug(@RequestBody MockCallDebugForm form) {
        AppMockDebugResult result = appMockCallService.debug(appMockCallWebMapper.web2vo(form.getAppMockCall()), form.getScriptTest());
        return R.builderOk().data("result", result).build();
    }
}
