package cn.icanci.loopstack.amc.web.controller.config;

import cn.icanci.loopstack.amc.biz.service.TeamService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.TeamVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.TeamQueryForm;
import cn.icanci.loopstack.amc.web.mapper.TeamWebMapper;
import cn.icanci.loopstack.amc.web.model.Team;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/07 18:17
 */
@RestController
@RequestMapping("/amc/team")
public class TeamController {
    @Resource
    private TeamService   teamService;

    @Resource
    private TeamWebMapper teamWebMapper;

    @PostMapping("query")
    public R query(@RequestBody TeamQueryForm form) {
        PageList<TeamVO> queryPage = teamService.queryPage(teamWebMapper.web2vo(form.getTeam()), form.getPaginator().getCurrentPage(), form.getPaginator().getPageSize());
        PageList<Team> ret = new PageList<>(teamWebMapper.vos2webs(queryPage.getData()), queryPage.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }

    @PostMapping("save")
    public R save(@RequestBody Team team) {
        teamService.save(teamWebMapper.web2vo(team));
        return R.builderOk().build();
    }

    @GetMapping("validateTeamId/{teamId:.*}")
    public R validateTeamId(@PathVariable("teamId") String teamId) {
        TeamVO team = teamService.queryByTeamId(teamId);
        return R.builderOk().data("result", team == null).build();
    }

    @GetMapping("validateTeamName/{teamName:.*}")
    public R validateTeamName(@PathVariable("teamName") String teamName) {
        TeamVO team = teamService.queryByTeamName(teamName);
        return R.builderOk().data("result", team == null).build();
    }

    @GetMapping("loadSelector")
    public R loadSelector() {
        List<TextValue> textValues = teamService.loadSelector();
        return R.builderOk().data("textValues", textValues).build();
    }

}
