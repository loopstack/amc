package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.LogOperate;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 13:01
 */
public class LogQueryForm extends BaseQueryForm {
    private static final long serialVersionUID = -934696142423210674L;

    private LogOperate        logOperate;

    public LogOperate getLogOperate() {
        return logOperate;
    }

    public void setLogOperate(LogOperate logOperate) {
        this.logOperate = logOperate;
    }

}
