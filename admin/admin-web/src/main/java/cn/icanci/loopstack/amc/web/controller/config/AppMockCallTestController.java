package cn.icanci.loopstack.amc.web.controller.config;

import cn.icanci.loopstack.amc.biz.service.AppMockCallTestService;
import cn.icanci.loopstack.amc.common.model.PageList;
import cn.icanci.loopstack.amc.common.model.TextValue;
import cn.icanci.loopstack.amc.common.model.config.AppMockCallTestVO;
import cn.icanci.loopstack.amc.common.result.R;
import cn.icanci.loopstack.amc.web.form.AppMockCallTestQueryForm;
import cn.icanci.loopstack.amc.web.mapper.AppMockCallTestWebMapper;
import cn.icanci.loopstack.amc.web.model.AppMockCallTest;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/06 21:11
 */
@RestController
@RequestMapping("/amc/appMockCallTest")
public class AppMockCallTestController {
    @Resource
    private AppMockCallTestService   appMockCallTestService;
    @Resource
    private AppMockCallTestWebMapper appMockCallTestWebMapper;

    @PostMapping("query")
    public R query(@RequestBody AppMockCallTestQueryForm form) {
        PageList<AppMockCallTestVO> queryPage = appMockCallTestService.queryPage(appMockCallTestWebMapper.web2vo(form.getAppMockCallTest()), form.getPaginator().getCurrentPage(),
            form.getPaginator().getPageSize());
        PageList<AppMockCallTest> ret = new PageList<>(appMockCallTestWebMapper.vos2webs(queryPage.getData()), queryPage.getPaginator());
        return R.builderOk().data("queryPage", ret).build();
    }

    @PostMapping("save")
    public R save(@RequestBody AppMockCallTest appMock) {
        appMockCallTestService.save(appMockCallTestWebMapper.web2vo(appMock));
        return R.builderOk().build();
    }

    @GetMapping("validateAppMockCallTestName/{mockTestName:.*}")
    public R validateAppMockCallTestName(@PathVariable("mockTestName") String mockTestName) {
        AppMockCallTestVO appMockTest = appMockCallTestService.validateAppMockCallName(mockTestName);
        return R.builderOk().data("result", appMockTest == null).build();
    }

    @GetMapping("loadSelector")
    public R loadSelector() {
        List<TextValue> textValues = appMockCallTestService.loadSelector();
        return R.builderOk().data("textValues", textValues).build();
    }
}
