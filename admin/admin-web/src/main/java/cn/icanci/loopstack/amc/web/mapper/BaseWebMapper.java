package cn.icanci.loopstack.amc.web.mapper;

import java.util.Collection;
import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 17:20
 */
public interface BaseWebMapper<T, R> {

    R web2vo(T t);

    List<R> webs2vos(List<T> ts);

    T vo2web(R r);

    List<T> vos2webs(Collection<R> rs);
}
