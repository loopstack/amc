package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.App;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/08 10:28
 */
public class AppQueryForm extends BaseQueryForm {
    /**
     * App 查询信息
     */
    private App app;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }
}
