package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.common.model.Paginator;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 17:00
 */
public class BaseQueryForm implements Serializable {
    private static final long serialVersionUID = -2325040090136131269L;

    private Paginator         paginator;

    public Paginator getPaginator() {
        return paginator;
    }

    public void setPaginator(Paginator paginator) {
        this.paginator = paginator;
    }
}
