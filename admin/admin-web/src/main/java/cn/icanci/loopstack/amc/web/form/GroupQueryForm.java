package cn.icanci.loopstack.amc.web.form;

import cn.icanci.loopstack.amc.web.model.Group;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/07 18:18
 */
public class GroupQueryForm extends BaseQueryForm {
    /**
     * group 查询信息
     */
    private Group group;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
