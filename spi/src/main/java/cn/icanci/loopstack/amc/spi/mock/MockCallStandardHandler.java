package cn.icanci.loopstack.amc.spi.mock;

/**
 * Mock Call 标准接口
 * 
 * @author icanci
 * @since 1.0 Created in 2023/01/17 13:45
 */
public abstract class MockCallStandardHandler {
    // TODO 暂时用不到
    //    @Resource
    //    protected LogService logService;

    /**
     * 执行处理器
     * 
     * @param request 请求(JSON格式的请求数据)
     * @return Object 执行返回值
     * @throws Exception Exception
     */
    public abstract Object execute(Object request) throws Exception;
}
