package cn.icanci.loopstack.amc.common.model.config;

import cn.icanci.loopstack.amc.common.enums.MethodEnum;
import cn.icanci.loopstack.script.enums.ScriptTypeEnum;

import java.util.StringJoiner;

/**
 * Mock的数据模型
 * 
 * @author icanci
 * @since 1.0 Created in 2023/01/14 20:01
 */
public class AppMockCallVO extends BaseVO {
    /**
     * mockName
     */
    private String         mockName;
    /**
     * 脚本关联的uuid
     */
    private String         appUuid;
    /**
     * 脚本执行类型
     */
    private ScriptTypeEnum scriptType;
    /**
     * 执行的脚本内容
     */
    private String         script;
    /**
     * 脚本的请求方法
     */
    private MethodEnum     method;

    public String getMockName() {
        return mockName;
    }

    public void setMockName(String mockName) {
        this.mockName = mockName;
    }

    public String getAppUuid() {
        return appUuid;
    }

    public void setAppUuid(String appUuid) {
        this.appUuid = appUuid;
    }

    public ScriptTypeEnum getScriptType() {
        return scriptType;
    }

    public void setScriptType(ScriptTypeEnum scriptType) {
        this.scriptType = scriptType;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public MethodEnum getMethod() {
        return method;
    }

    public void setMethod(MethodEnum method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("appUuid=" + appUuid).add("scriptType=" + scriptType).add("script=" + script).add("method=" + method).toString();
    }
}
