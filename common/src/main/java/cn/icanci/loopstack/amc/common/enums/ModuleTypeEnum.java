package cn.icanci.loopstack.amc.common.enums;

/**
 * @author icanci
 * @since 1.0 Created in 2022/12/11 17:37
 */
public enum ModuleTypeEnum {
                            /**
                             * AMC_GROUP
                             */
                            AMC_GROUP("AMC_GROUP", "事业群"),
                            /**
                             * AMC_TEAM
                             */
                            AMC_TEAM("AMC_TEAM", "项目组"),
                            /**
                             * AMC_APP
                             */
                            AMC_APP("AMC_APP", "项目"),
                            /**
                             * AMC_APP_MOCK_CALL
                             */
                            AMC_APP_MOCK_CALL("AMC_APP_MOCK_CALL", "项目MockCall"),
                            /**
                             * AMC_APP_MOCK_CALL_TEST
                             */
                            AMC_APP_MOCK_CALL_TEST("AMC_APP_MOCK_CALL_TEST", "项目MockCallTest"),
                            /**
                             * AMC_APP_MOCK_CALLBACK
                             */
                            AMC_APP_MOCK_CALLBACK("AMC_APP_MOCK_CALLBACK", "项目MockCallBack"),

    ;

    ModuleTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
