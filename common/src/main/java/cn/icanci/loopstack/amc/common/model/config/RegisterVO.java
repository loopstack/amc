package cn.icanci.loopstack.amc.common.model.config;

import java.util.Date;
import java.util.StringJoiner;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/22 21:35
 */
public class RegisterVO extends BaseVO {
    /**
     * SDK 服务ip地址
     */
    private String  clientAddress;
    /**
     * SDK 服务端口地址
     */
    private Integer clientPort;
    /**
     * SDK 服务服务唯一标识
     */
    private String  appId;
    /**
     * 服务注册时间
     */
    private Date    registerTime;
    /**
     * 上次注册更新时间
     */
    private Date    lastUpdateTime;

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public void setClientPort(Integer clientPort) {
        this.clientPort = clientPort;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("clientAddress=" + clientAddress).add("clientPort=" + clientPort).add("appId=" + appId).add("registerTime=" + registerTime)
            .add("lastUpdateTime=" + lastUpdateTime).toString();
    }
}
