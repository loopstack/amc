package cn.icanci.loopstack.amc.common.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 16:38
 */
public class TextValue<T> implements Serializable {
    private static final long serialVersionUID = -3727976523289870284L;

    private String            label;
    private Object            value;
    private T                 data;
    private List              list;

    public TextValue(String label, Object value) {
        this.label = label;
        this.value = value;
    }

    public TextValue(String label, Object value, T data) {
        this.label = label;
        this.value = value;
        this.data = data;
    }

    public TextValue(String label, Object value, T data, List list) {
        this.label = label;
        this.value = value;
        this.data = data;
        this.list = list;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
