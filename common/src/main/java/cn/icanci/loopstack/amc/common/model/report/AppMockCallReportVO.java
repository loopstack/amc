package cn.icanci.loopstack.amc.common.model.report;

import cn.icanci.loopstack.amc.common.model.config.BaseVO;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/18 11:46
 */
@HeadRowHeight(18)
public class AppMockCallReportVO extends BaseVO {
    /**
     * mock 过程是否成功
     */
    @ExcelProperty(value = { "mock结果" })
    private boolean success;
    /**
     * mockName
     */
    @ExcelProperty(value = { "mockName" })
    private String  mockName;
    /**
     * mockUuid
     */
    @ExcelProperty(value = { "mockUuid" })
    private String  mockUuid;
    /**
     * mock执行的请求
     */
    @ExcelProperty(value = { "mockRequest" })
    private String  mockRequest;
    /**
     * mock执行耗时
     */
    @ExcelProperty(value = { "执行耗时" })
    private long    runtime;
    /**
     * mock执行的异常信息
     */
    @ExcelProperty(value = { "mockErrorMessage" })
    private String  mockErrorMessage;
    /**
     * 真正mock返回的结果
     */
    @ExcelProperty(value = { "mockResponse" })
    private String  mockResponse;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMockName() {
        return mockName;
    }

    public void setMockName(String mockName) {
        this.mockName = mockName;
    }

    public String getMockUuid() {
        return mockUuid;
    }

    public void setMockUuid(String mockUuid) {
        this.mockUuid = mockUuid;
    }

    public String getMockRequest() {
        return mockRequest;
    }

    public void setMockRequest(String mockRequest) {
        this.mockRequest = mockRequest;
    }

    public String getMockErrorMessage() {
        return mockErrorMessage;
    }

    public void setMockErrorMessage(String mockErrorMessage) {
        this.mockErrorMessage = mockErrorMessage;
    }

    public String getMockResponse() {
        return mockResponse;
    }

    public void setMockResponse(String mockResponse) {
        this.mockResponse = mockResponse;
    }

    public long getRuntime() {
        return runtime;
    }

    public void setRuntime(long runtime) {
        this.runtime = runtime;
    }
}
