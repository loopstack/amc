package cn.icanci.loopstack.amc.common.model.config;

import java.util.StringJoiner;

/**
 * 项目组
 *
 * @author icanci
 * @since 1.0 Created in 2022/12/11 17:40
 */
public class TeamVO extends BaseVO {
    /**
     * 项目组id，唯一
     */
    private String teamId;
    /**
     * 项目组名字
     */
    private String teamName;
    /**
     * 事业群关联uuid
     */
    private String groupUuid;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getGroupUuid() {
        return groupUuid;
    }

    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("teamId=" + teamId).add("teamName=" + teamName).add("groupUuid=" + groupUuid).toString();
    }
}
