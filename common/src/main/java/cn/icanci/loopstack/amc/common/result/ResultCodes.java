package cn.icanci.loopstack.amc.common.result;

/**
 * code集合
 * 
 * @author icanci
 * @since 1.0 Created in 2022/04/04 20:09
 */
public interface ResultCodes {
    /** 成功 */
    int SUCCESS            = 200;
    /** 400异常 */
    int FAIL_404           = 404;
    /** 500异常 */
    int FAIL_500           = 500;
    /** 账号异常 */
    int ACCOUNT_ERROR_CODE = 1000;
    /** 账号登录超时 */
    int LOGIN_TIME_OUT     = 1001;
    /** 默认失败原因 */
    int FAIL_SYSTEM        = 9999;
}
