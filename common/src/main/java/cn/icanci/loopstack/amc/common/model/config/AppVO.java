package cn.icanci.loopstack.amc.common.model.config;

import java.util.StringJoiner;

/**
 * 项目组项目
 *
 * @author icanci
 * @since 1.0 Created in 2022/12/11 17:42
 */
public class AppVO extends BaseVO {
    /**
     * 项目id，全局唯一
     */
    private String appId;
    /**
     * 项目名字
     */
    private String appName;
    /**
     * 项目组关联uuid
     */
    private String teamUuid;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getTeamUuid() {
        return teamUuid;
    }

    public void setTeamUuid(String teamUuid) {
        this.teamUuid = teamUuid;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("appId=" + appId).add("appName=" + appName).add("teamUuid=" + teamUuid).toString();
    }
}
