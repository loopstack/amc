package cn.icanci.loopstack.amc.common.enums;

/**
 * @author icanci
 * @since 1.0 Created in 2022/12/24 19:51
 */
public enum BooleanEnum {
                         /**
                          * Y
                          */
                         YES(1, "YES"),
                         /**
                          * N
                          */
                         NO(0, "NO");

    BooleanEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * code
     */
    private final int    code;
    /**
     * desc
     */
    private final String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
