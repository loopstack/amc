package cn.icanci.loopstack.amc.common.enums;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 11:45
 */
public enum MethodEnum {
                        /**
                         * GET
                         */
                        GET("GET"),
                        /**
                         * POST
                         */
                        POST("POST"),;

    MethodEnum(String code) {
        this.code = code;
    }

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
