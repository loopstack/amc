package cn.icanci.loopstack.amc.common.enums;

/**
 * 日志操作类型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/11 16:15
 */
public enum LogOperatorTypeEnum {
                                 /**
                                  * CREATE
                                  */
                                 CREATE("CREATE", "创建"),
                                 /**
                                  * UPDATE
                                  */
                                 UPDATE("UPDATE", "更新"),
                                 /**
                                  * 删除
                                  */
                                 DELETE("DELETE", "删除"),
                                 /**
                                  * 发布
                                  */
                                 PUBLISH("PUBLISH", "发布"),

    ;

    LogOperatorTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
