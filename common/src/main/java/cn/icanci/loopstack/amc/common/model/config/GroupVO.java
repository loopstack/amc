package cn.icanci.loopstack.amc.common.model.config;

import java.util.StringJoiner;

/**
 * 事业群
 * 
 * @author icanci
 * @since 1.0 Created in 2022/12/11 17:39
 */
public class GroupVO extends BaseVO {
    /**
     * 事业群组id，唯一
     */
    private String groupId;
    /**
     * 事业群组名字
     */
    private String groupName;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("groupId=" + groupId).add("groupName=" + groupName).toString();
    }
}
