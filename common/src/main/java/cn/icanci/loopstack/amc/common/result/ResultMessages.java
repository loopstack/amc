package cn.icanci.loopstack.amc.common.result;

/**
 * message集合
 * 
 * @author icanci
 * @since 1.0 Created in 2022/04/04 20:09
 */
public interface ResultMessages {
    String ACCOUNT_ERROR_MESSAGE = "账号%s异常，请重新登录";
}
