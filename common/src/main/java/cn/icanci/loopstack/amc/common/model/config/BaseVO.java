package cn.icanci.loopstack.amc.common.model.config;

import java.util.Date;
import java.util.StringJoiner;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 基础模型
 * 
 * @author icanci
 * @since 1.0 Created in 2022/12/11 17:37
 */
public class BaseVO {
    /**
     * 数据库id
     */
    @ExcelIgnore
    private String  id;

    /**
     * 雪花算法随机UUID
     */
    @ExcelIgnore
    private String  uuid;

    /**
     * 功能描述
     */
    @ExcelIgnore
    private String  desc;

    /**
     * 创建时间
     */
    @ExcelIgnore
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date    createTime;

    /**
     * 更新时间
     */
    @ExcelIgnore
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date    updateTime;

    /**
     * 状态 0有效，1无效
     */
    @ExcelIgnore
    private Integer isDelete;

    /**
     * 环境
     */
    private String  env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("id=" + id).add("uuid=" + uuid).add("desc=" + desc).add("createTime=" + createTime).add("updateTime=" + updateTime)
            .add("isDelete=" + isDelete).add("env=" + env).toString();
    }
}
